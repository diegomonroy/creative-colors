<?php
$style = '';
if ( is_front_page() ) : $style = 'home'; endif;
if ( is_page( array(
	'ojos',
	'delineadores',
	'sombras',
	'mascaras',
	'cejas',
	'labios',
	'delineadores-de-labios',
	'labiales',
	'rostro'
) ) ) :
	$style = 'white_bg';
endif;
?>
<?php get_template_part( 'part', 'banner' ); ?>
<!-- Begin Content -->
	<section class="content <?php echo $style; ?> wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->