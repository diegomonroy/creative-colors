<!-- Begin Top -->
	<section class="top wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns text-center">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-7 columns">
				<?php dynamic_sidebar( 'menu' ); ?>
			</div>
			<div class="small-12 medium-2 columns text-center">
				<?php dynamic_sidebar( 'languages' ); ?>
				<?php dynamic_sidebar( 'social_media' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->