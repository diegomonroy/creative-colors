<!-- Begin Content -->
	<section class="content white_bg" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 medium-3 columns text-center">
				<?php dynamic_sidebar( 'left' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->