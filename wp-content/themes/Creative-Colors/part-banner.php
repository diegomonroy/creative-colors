<!-- Begin Banner -->
	<section class="banner wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'nuestra-compania' ) ) ) : dynamic_sidebar( 'banner_nuestra_compania' ); endif; ?>
				<?php if ( is_page( array( 'vision' ) ) ) : dynamic_sidebar( 'banner_nuestra_compania' ); endif; ?>
				<?php if ( is_page( array( 'mision' ) ) ) : dynamic_sidebar( 'banner_nuestra_compania' ); endif; ?>
				<?php if ( is_page( array( 'quienes-somos' ) ) ) : dynamic_sidebar( 'banner_nuestra_compania' ); endif; ?>
				<?php if ( is_page( array( 'perfil-de-la-empresa' ) ) ) : dynamic_sidebar( 'banner_nuestra_compania' ); endif; ?>
				<?php if ( is_page( array( 'red-global' ) ) ) : dynamic_sidebar( 'banner_nuestra_compania' ); endif; ?>
				<?php if ( is_page( array( 'donde-estamos' ) ) ) : dynamic_sidebar( 'banner_nuestra_compania' ); endif; ?>
				<?php if ( is_page( array( 'operacion-comercial' ) ) ) : dynamic_sidebar( 'banner_nuestra_compania' ); endif; ?>
				<?php if ( is_page( array( 'ojos' ) ) ) : dynamic_sidebar( 'banner_ojos' ); endif; ?>
				<?php if ( is_page( array( 'cejas' ) ) ) : dynamic_sidebar( 'banner_cejas' ); endif; ?>
				<?php if ( is_page( array( 'labios' ) ) ) : dynamic_sidebar( 'banner_labios' ); endif; ?>
				<?php if ( is_page( array( 'rostro' ) ) ) : dynamic_sidebar( 'banner_rostro' ); endif; ?>
				<?php if ( is_page( array( 'contactenos' ) ) ) : dynamic_sidebar( 'banner_contactenos' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->