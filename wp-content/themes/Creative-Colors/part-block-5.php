<!-- Begin Block 5 -->
	<section class="block_5 wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_5' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 5 -->