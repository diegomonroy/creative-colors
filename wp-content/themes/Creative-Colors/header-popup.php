<!DOCTYPE html>
<html class="no-js" lang="<?php echo get_locale(); ?>">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="l7NLqWamqaPZiMbh8w51Ohx0tX4ODlA8jjG0ve6YNLU">
		<title><?php bloginfo(title); ?></title>
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo site_url(); ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php bloginfo(name); ?>">
		<meta property="og:description" content="<?php bloginfo(description); ?>">
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/build/logo_ogp.png">
		<link rel="image_src" href="<?php echo get_template_directory_uri(); ?>/build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<?php wp_head(); ?>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/build/favicon.ico">
		<!-- Begin Google Analytics -->
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131117411-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-131117411-1');
		</script>
		<!-- End Google Analytics -->
		<!-- Begin Google Tag Manager -->
		<!-- Global site tag (gtag.js) - Google Ads: 798019065 -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=AW-798019065"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'AW-798019065');
		</script>
		<!-- End Google Tag Manager -->
		<?php if ( is_page( array( 'contactenos' ) ) ) : ?>
		<!-- Begin Google Tag Manager Contáctenos -->
		<!-- Event snippet for CONTACTO conversion page
		In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
		<script>
			function gtag_report_conversion(url) {
				var callback = function () {
					if (typeof(url) != 'undefined') {
						window.location = url;
					}
				};
				gtag('event', 'conversion', {
					'send_to': 'AW-798019065/ucxjCMzf0ZYBEPmbw_wC',
					'event_callback': callback
				});
				return false;
			}
		</script>
		<!-- End Google Tag Manager Contáctenos -->
	</head>
	<body>