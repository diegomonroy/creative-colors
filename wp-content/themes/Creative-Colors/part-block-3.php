<!-- Begin Block 3 -->
	<section class="block_3 wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_3' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 3 -->