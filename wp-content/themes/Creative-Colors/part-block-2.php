<!-- Begin Block 2 -->
	<section class="block_2 wow fadeIn" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_2' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 2 -->